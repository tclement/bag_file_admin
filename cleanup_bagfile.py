import logging
import os
import shutil
import time
import yaml
from os.path import exists

from rosbag.bag import Bag, ROSBagException


def setup_logging_files():
    logging_file = LOGGING_PATH + LOGGING_FILE_NAME
    if exists(logging_file):
        time_string = time.strftime("%Y%m%d_%H%M%S")
        shutil.move(
            logging_file,
            LOGGING_PATH + time_string + "_bag_cleanup.log",
        )
    logging.basicConfig(
        format="%(asctime)s %(levelname)s:%(message)s",
        filename=logging_file,
        level=logging.DEBUG,
    )
    remove_old_log_files(LOGGING_PATH, 10)


def remove_old_log_files(log_directory, number_of_files=10):
    try:
        list_of_files = [
            file for file in os.listdir(log_directory) if file.endswith(".log")
        ]
        while len(list_of_files) > number_of_files:
            full_paths = [os.path.join(log_directory, file) for file in list_of_files]
            oldest_file = min(full_paths, key=os.path.getctime)
            os.remove(oldest_file)
            list_of_files = [_ for _ in os.listdir(log_directory) if _.endswith(".log")]
    except OSError as e:
        logging.warning(e.__class__.__name__ + str(e))


def message_count_greater_than_one(topics_dict):
    msg_count_list = []
    for value in topics_dict.values():
        msg_count_list.append(value.message_count)
    return all(x > 1 for x in msg_count_list)


def cleanup_bag_files():
    files = os.listdir(BAGPATH)
    for f in files:
        if f.endswith(".bag"):
            full_file_name_with_path = BAGPATH + f
            try:
                types_and_topics_in_bag = Bag(
                    full_file_name_with_path, "r"
                ).get_type_and_topic_info(topic_filters=topics)

                all_topics_in_bag_bool = all(
                    x in types_and_topics_in_bag.topics for x in topics
                )
                # types_and_topics_in_bag.topics is a python dictionary
                # if it is empty there will be no information in the bag file
                # relevant to the topics that were filtered on
                # Those bag files will be considered bad and moved to temp dir
                # bags with only one message per topic will also be moved to temp dir
                if not types_and_topics_in_bag.topics:
                    shutil.move(full_file_name_with_path, f"{TMPPATH}{f}.NoTopics")
                    logging.info(f + " Bad Bag")

                # some bag files may have some but not all the topics.
                # move bag files that are like this
                # these bag files are ones where something went wrong
                elif not all_topics_in_bag_bool:
                    shutil.move(full_file_name_with_path, f"{TMPPATH}{f}.MissingTopics")
                    logging.info(f + " Bad Bag")
                elif message_count_greater_than_one(types_and_topics_in_bag.topics):
                    logging.info(f + " Good Bag")
                else:
                    shutil.move(full_file_name_with_path, TMPPATH + f)
                    logging.info(f + " No Messages Bag")

            # if the file is corrupt or not indexed then it will throw exception
            # it will be considered bad and be moved to temp directory
            except ROSBagException as e:
                shutil.move(full_file_name_with_path, TMPPATH + f)
                logging.warning(e.value + ": " + full_file_name_with_path + " Bad Bag")


if __name__ == "__main__":
    try:
        with open("config.yaml", "r") as ymlfile:
            cfg = yaml.safe_load(ymlfile)
    except FileNotFoundError as e:
        print("Config File Not Found:" + str(e))
    except yaml.YAMLError as e:
        print("Error in configuration file:", e)
    else:
        topics = cfg["topics"]
        BAGPATH = cfg["BAGPATH"]
        TMPPATH = cfg["TMPPATH"]
        LOGGING_PATH = cfg["LOGGING_PATH"]
        LOGGING_FILE_NAME = cfg["LOGGING_FILE_NAME"]
        setup_logging_files()
        cleanup_bag_files()

import glob
import json
import os
import yaml
from collections import defaultdict
from datetime import datetime

import matplotlib.pyplot as plt
import pandas as pd
import rosbag
from rospy_message_converter import json_message_converter


def get_msgs_in_bag_to_json(bagfile):
    bag_file_serial_number = bagfile[-15:-4]
    bag_in = rosbag.Bag(bagfile)
    topics_dict = defaultdict(list)
    for topic, msg, t in bag_in.read_messages(topics):
        json_str = json_message_converter.convert_ros_message_to_json(msg)
        json_dict = json.loads(json_str)
        json_dict["header"]["time_stamp"]["secs"] = t.to_sec()
        topics_dict[topic].append(json_dict)

    for k, v in topics_dict.items():
        topic_file_name = k.split("/")[-1]
        try:
            with open(
                f"{BAG_EXTRACT_OUTPUT}{bag_file_serial_number}/{topic_file_name}.json",
                "w",
                newline="",
            ) as topicFile:

                topicFile.write(json.dumps(v, indent=2))
                topicFile.write("\n")
        except FileNotFoundError as e:
            print("File Not Found:" + str(e))


def get_msgs_in_bag_to_csv(bagfile):
    bag_file_serial_number = bagfile[-15:-4]
    bag_in = rosbag.Bag(bagfile)
    for topic in topics:
        df = pd.DataFrame()
        topic_name = topic.split("/")[-1]
        for top, msg, t in bag_in.read_messages(topic):
            if msg:
                df = df.append(
                    get_msg_fields(msg, t),
                    ignore_index=True,
                )

        try:
            if not df.empty:
                df.to_csv(
                    f"{BAG_EXTRACT_OUTPUT}{bag_file_serial_number}/{topic_name}.csv",
                    index=False,
                )
                df.plot(x="date", y=["data"])
                plt.title(topic_name)
                plt.savefig(
                    f"{BAG_EXTRACT_OUTPUT}{bag_file_serial_number}/{topic_name}.jpg"
                )
            else:
                with open(
                    f"{BAG_EXTRACT_OUTPUT}{bag_file_serial_number}/{topic_name}_EMPTY.txt",
                    "w",
                    newline="",
                ) as bad_topic_file:
                    bad_topic_file.write("No data in bag file")

        except FileNotFoundError as e:
            print("File Not Found:" + str(e))


def get_msg_fields(msg, t):
    measurement_number = msg.measurement_number
    data = msg.data
    criteria_met = msg.criteria_met
    error_number = msg.error_number
    timestamp = t.to_sec()
    dt_object = datetime.fromtimestamp(timestamp)
    return {
        "measurement_number": measurement_number,
        "data": data,
        "criteria_met": criteria_met,
        "error_number": error_number,
        "timestamp": timestamp,
        "date": dt_object,
    }


def create_output_directory(bagfile):
    bag_file_serial_number = bagfile[-15:-4]
    save_folder = f"{BAG_EXTRACT_OUTPUT}{bag_file_serial_number}"
    try:
        if not os.path.exists(save_folder):
            os.makedirs(save_folder)
    except OSError:
        print(f"Creation of the directory {save_folder} FAILED")


def process_bag_files():
    bagfiles = glob.glob(f"{BAGPATH}*.bag")
    for bagfile in bagfiles:
        create_output_directory(bagfile)
        get_msgs_in_bag_to_csv(bagfile)
        get_msgs_in_bag_to_json(bagfile)


if __name__ == "__main__":
    try:
        with open("config.yaml", "r") as ymlfile:
            cfg = yaml.safe_load(ymlfile)
    except FileNotFoundError as e:
        print("Config File Not Found:" + str(e))
    except yaml.YAMLError as e:
        print("Error in configuration file:", e)
    else:
        topics = cfg["topics"]
        BAGPATH = cfg["BAGPATH"]
        BAG_EXTRACT_OUTPUT = cfg["BAG_EXTRACT_OUTPUT"]

        process_bag_files()
